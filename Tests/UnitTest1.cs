using Assignment.Objects.Furniture;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var furn1 = new Couch("cotton", 100, true);
			var furn2 = furn1.GenericClone();

			//changing a copy to see that it is a new object
			furn2.HasArmSupport = false;
			furn2.Material += "_new";
			furn2.Price += 2;

			Assert.AreNotEqual(furn1.Material, furn2.Material);
			Assert.AreNotEqual(furn1.Price, furn2.Price);
			Assert.AreNotEqual(furn1.HasArmSupport, furn2.HasArmSupport);

			var furn3 = new Couch("cotton", 100, true);
			var furn4 = (Couch)furn3.Clone();

			//changing a copy to see that it is a new object
			furn4.HasArmSupport = false;
			furn4.Material += "_new";
			furn4.Price += 2;

			Assert.AreNotEqual(furn3.Material, furn4.Material);
			Assert.AreNotEqual(furn3.Price, furn4.Price);
			Assert.AreNotEqual(furn3.HasArmSupport, furn4.HasArmSupport);
		}
	}
}