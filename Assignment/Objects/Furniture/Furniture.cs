﻿using Assignment.Interfaces;

namespace Assignment.Objects.Furniture
{
	/// <summary>
	/// Мебель
	/// </summary>
	public abstract class Furniture : ICloneable, IMyCloneable<Furniture>
	{
		public Decimal Price { get; set; }

		internal Furniture(decimal price)
		{
			Price = price;
		}

		public object Clone()
		{
			return GenericClone();
		}

		public abstract Furniture GenericClone();
	}
}
