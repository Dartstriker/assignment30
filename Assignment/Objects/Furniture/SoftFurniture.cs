﻿using Assignment.Interfaces;

namespace Assignment.Objects.Furniture
{
	/// <summary>
	/// Мягкая мебель
	/// </summary>
	public class SoftFurniture : Furniture, ICloneable, IMyCloneable<SoftFurniture>
	{
		public string Material { get; set; }
		
		internal SoftFurniture(string material, decimal price) : base(price)
		{
			Material = material;
		}

		public object Clone()
		{
			return GenericClone();
		}

		public override SoftFurniture GenericClone()
		{
			return new SoftFurniture(Material, Price);
		}
	}
}
