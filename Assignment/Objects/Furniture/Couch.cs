﻿using Assignment.Interfaces;

namespace Assignment.Objects.Furniture
{
	/// <summary>
	/// Диван
	/// </summary>
	public class Couch : SoftFurniture, ICloneable, IMyCloneable<Couch>
	{
		public bool HasArmSupport;

		public Couch(string material, decimal price, bool hasArmSupport) : base(material, price)
		{
			HasArmSupport = hasArmSupport;
		}

		public override Couch GenericClone()
		{
			return new Couch(Material, Price, HasArmSupport);
		}

		public object Clone()
		{
			return GenericClone();
		}
	}
}
