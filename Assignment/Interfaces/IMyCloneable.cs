﻿namespace Assignment.Interfaces
{
	internal interface IMyCloneable<T>
	{
		T GenericClone();
	}
}
